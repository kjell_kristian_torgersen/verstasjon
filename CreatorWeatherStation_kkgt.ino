#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <ThingSpeak.h>
#include <Adafruit_BME280.h>      // BME280 sensor bibliotek
#include <ESP8266WiFi.h>          // ESP8266 Core WiFi Library
#include <DNSServer.h>            // Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     // Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          // https://github.com/tzapu/WiFiManager WiFi Configuration Magic

#define SECONDS 1000000 // Antall ticks per sekund for deep sleep

ADC_MODE(ADC_VCC);  // tipper dette er for å måle VCC med ADC
WiFiClient client;
WiFiManager wifiManager; //initialisering av WiFi Manager
Adafruit_BME280 bme; // I2C initialisering av

const char* server = "api.thingspeak.com";
const unsigned long myChannelNumber =; // finnes på thingsspeak.com etter man har laget kanal
const char * myWriteAPIKey = ""; // 
#error Legg inn Channel ID og API nøkkel over og fjern denne linjen!

unsigned long delayTime = 1000; // Delay som kreves for hva?

void setup() {
  Serial.begin(9600);
  wifiManager.autoConnect("APTest_kkgt"); //Prøver å koble til wifi, hvis den feiler kan du gå til 192.168.4.1 og konfigurere
  Serial.println(F("BME280 test"));
  bool status;
  // default settings
  // (you can also pass in a Wire library object like &Wire2)
  status = bme.begin();
  if (!status) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
  Serial.println("-- Default Test --");
  
  Serial.println();
  delay(100); // let sensor boot up
}

void loop() {
  printValues();
  delay(delayTime); // hvorfor ??? Noe som må gjøre seg ferdig før deep sleep?
  ESP.deepSleep(20*SECONDS);
}

void printValues() {
  float voltage = (ESP.getVcc() / 1000.0F); // Mål innspenning først

  if (voltage > 2.5) { // batterispenning OK, mål med sensor og send
    float temperature = bme.readTemperature();
    float humidity = bme.readHumidity();
    float pressure = bme.readPressure() / 100.0F;

    // Utskrift av verdier til serieport
    Serial.print("Temperature = ");
    Serial.print(temperature);
    Serial.println(" *C");

    Serial.print("Pressure = ");
    Serial.print(pressure);
    Serial.println(" hPa");

    Serial.print("Humidity = ");
    Serial.print(humidity);
    Serial.println(" %");

    Serial.print("Battery = ");
    Serial.print(voltage);
    Serial.println(" V");
    Serial.println();

    // Opplasting til thingsspeak. Sjekk at rekkefølgen her stemmer med det som er lagt inn på thingsspeak.com
    ThingSpeak.begin(client); 
    ThingSpeak.setField(1, temperature);
    ThingSpeak.setField(2, humidity);
    ThingSpeak.setField(3, pressure);
    ThingSpeak.setField(4, voltage);
    ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
    
  } else { // batterispenning lav... vent til den har blitt høyere
    Serial.print("Avoid transmitting due to low battery voltage: ");
    Serial.print(voltage);
    Serial.println(" V");
  }
}

