# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
	 Installer Arduino Studio 1.8.5
* Configuration
	 1. Legg til http://arduino.esp8266.com/stable/package_esp8266com_index.json som board manager URL i Preferences Arduino studio
	 2. Installer støtte for ESP8266
	 3. Velg Wemos D1 & R2 Mini
	 4. Installer Dependencies med Library Manager
* Dependencies (noen er sikkert allerede installert)
	 1. Wire
	 2. SPI
	 3. Adafruit Unified Sensor
	 4. ThingSpeak
	 5. Adafruit BME280 Library
	 6. ESP8266WiFi
	 7. ESP8266WebServer
	 8. WhareHauoraWiFiManager
	 9. DNSServer
* Database configuration
	 1. Lag konto på thingspeak
	 2. Lag kanal med måleverdier
	 3. Husk kanal id og skrive nøkkel
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact